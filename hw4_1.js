
// Задача 1. 
// Напишите функцию, которая возвращает все числа от одного до ста, 
// которые меньше, чем квадратный корень числа, которое ввёл пользователь. 
// Выведите числа в консоль. Ввод числа проводите через prompt().

let usersInt = prompt('enter digit', '10');
let intForFunc;
function lessThenSquare (intForFunc) {
    let sqtrCounter = Math.sqrt(intForFunc);
    for (let i = 0; i <= 100; i++) {
        if (i < sqtrCounter) {
            console.log(i + '\n');
        }
    }
} 

lessThenSquare(usersInt);