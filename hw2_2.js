// 2. В Одессе открыли наземное метро и ввели тарифы на перевозку ручной клади. Выделили три категории оплаты по массе:
// 1. 0 - 5 кг - 3 грн за килограмм.
// 2. 5 -10 кг - 5 грн за килограмм.
// 3. 10 -15 кг - 10 грн за килограмм.
// Через три дня приехал турист с Англии. Выведите в консоль сколько он должен заплатить фунтов стерлингов за свою кладь, 
// массу которой он вводит в prompt(), с учётом, что он вводит её в фунтах. 
// Курс валюты на момент ввода фиксированный и составляет 2.88 фунтов стерлингов за 100 украинских гривен.
// 1 кг = 2,20462 фунтов
// Учтите, что он джентельмен и может ввести массу прописью. Уведомьте его о том, что можно вводить только числа.

let weight;
let price;
let totalPrice;
let kiloWeight; 
const poundToKilos = 2.20462; // 1 kilo to pounds
let currencyRate;
let sterlingPounds = 0.0288; //pounds for 1 uah
currencyRate = sterlingPounds;

weight = prompt('Please add your weight in kg?', '1');

if (weight != null && isNaN(weight) == false) {
    kiloWeight = weight / poundToKilos;
    switch (true) {
        case kiloWeight > 0 && kiloWeight <= 5:
            console.log(kiloWeight);
            price = 3;
            totalPrice = kiloWeight * price * currencyRate;
            console.log(totalPrice.toFixed(2) + ' pounds u need to pay'); //toFixed - cut up to 2 symbols after comma
            break;
        case kiloWeight > 5 && kiloWeight <= 10:
            price = 5;
            totalPrice = kiloWeight * price * currencyRate;
            console.log(totalPrice.toFixed(2) + ' pounds u need to pay'); //toFixed - cut up to 2 symbols after comma
            break;
        case kiloWeight > 10 && kiloWeight <= 15:
            price = 10;
            totalPrice = kiloWeight * price * currencyRate;
            console.log(totalPrice.toFixed(2) + ' pounds u need to pay'); //toFixed - cut up to 2 symbols after comma
            break;
        default:
            console.log('this weight is out of allowed');    
    }
} else {
    console.log('Please type weight only in digit presentation. Refresh page and try again.');
}


