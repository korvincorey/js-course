// На декартовой системе координат размещена окружность так, что её центр совпадает с
// началом отсчёта осей. Окружность вписана в квадрат. Стороны квадрата
// перпендикулярны осям (см. прикреплённое изображение). Радиус окружности равен 5
// единиц, а сторона квадрата - 10.
// Пользователь вводит две координаты точки с помощью prompt(). Выясните и выведите в
// alert() находится ли эта точка в заштрихованной области.

let circleRadius = 5;
let quadrantSide = 10;
let hatchedZone;

let axisValueX = prompt('Enter X axis value', '10');
let axisValueY = prompt('Enter Y axis value', '10');

if (axisValueX > 0 && axisValueX <= 5 && axisValueY > 0 && axisValueY <= 5 && axisValueX**2 + axisValueY**2 >= circleRadius**2) {
    alert('tochka s coordinatami: ' + axisValueX + ', ' + axisValueY + ' zashla v zonu');
} else {
    alert('this values out of zone');
}
