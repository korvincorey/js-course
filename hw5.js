// Интернет-магазин канцтоваров.
// Напишите функцию, которая принимает название товара, его цену и категорию, добавляет этот товар в массив, 
// где каждый товар это объект и возвращает массив всех товаров. Товаров может быть сколько угодно. 
// [
//   { 
//     name: 'pen',
//     price: 2,
//     category: pens
//   }
// ] 
// - Напишите функцию, которая фильтрует товары по цене от и до и возвращает новый массив 
// только с товарами выбранного ценового диапазона или пустой массив. 
// - Напишите функцию, которая фильтрует товары по категории и возвращает новый массив 
// только с товарами выбранной категории, если она есть или пустой массив. 
// - Напишите функцию, которая возвращает количесто товаров в категории.

let itemsArray = [];

function itemsFill (nameArg, priceArg, categoryArg) {
    itemsArray.push({name: nameArg, price: priceArg, category: categoryArg});
    return itemsArray;
}

function filterByPrice (array, lowestPrice, highestPrice) {
    let filteredArray = array.filter(item => item.price >= lowestPrice && item.price <= highestPrice);
    return filteredArray;
}

function filterAndReturnByCategory (array, category) {
    let filteredArray = array.filter(item => item.category == category);
    return filteredArray;
}

function categoryCount (array, category) {
    let filteredArray = array.filter(item => item.category == category);
    let categoryLength = filteredArray.length;
    return categoryLength;
}

itemsFill('mac', 1000, 'apple');
itemsFill('galaxy', 200, 'samsung');
itemsFill('mate', 500, 'huawei');
itemsFill('mac', 900, 'apple');

let priceResult = filterByPrice(itemsArray, 500, 9000);
let categoryResult = filterAndReturnByCategory(itemsArray, 'apple');
let categoryCountResult = categoryCount(itemsArray, 'apple');

console.log(priceResult);
console.log(categoryResult);
console.log(categoryCountResult); 